import {Component, inject} from '@angular/core';
import {Router} from "@angular/router";
import {AsyncPipe} from "@angular/common";
import {StepService} from "../../../features/steps/step.service";

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [
    AsyncPipe
  ],
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.scss'
})
export class NavbarComponent {
  private router = inject(Router);
  protected stepService = inject(StepService);

  onStepNavigate(step: number) {
    this.router.navigate([`/step/${step}`]);
  }
}
