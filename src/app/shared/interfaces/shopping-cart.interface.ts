export interface ShoppingCart {
  model: string;
  color: string;
  config: number | string;
  towHitch: boolean;
  yoke: boolean;
}
