import {Component, DestroyRef, inject, OnInit, signal, WritableSignal} from '@angular/core';
import {FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {StepService} from "../step.service";
import {Model, Color} from "../../car";
import {takeUntilDestroyed} from "@angular/core/rxjs-interop";

@Component({
  selector: 'app-step-one',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    CommonModule
  ],
  templateUrl: './step-one.component.html',
  styleUrl: './step-one.component.scss'
})
export class StepOneComponent implements OnInit {
  private readonly destroyRef = inject(DestroyRef);
  protected readonly stepService = inject(StepService);

  models: WritableSignal<Model[]> = signal([]);
  colors: WritableSignal<Color[]> = signal([]);
  stepOneForm: FormGroup = new FormGroup({
    modelSelect: new FormControl('', Validators.required),
    colorSelect: new FormControl('', Validators.required)
  });

  ngOnInit() {
    this.stepService.getModels()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(models => {
      this.models.set(models);
      this.patchForm();
    });

    this.stepOneForm.valueChanges
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(value => {
      const shoppingCart = this.stepService.shoppingCart;

      Object.keys(value).forEach(key => {
        if (this.stepOneForm.get(key)?.dirty && key === 'modelSelect') {
          if (this.stepOneForm.get('modelSelect')?.value !== shoppingCart.model) {
            this.stepService.updateStepTwoValidity(false);
            this.stepService.updateShoppingCart({
              model: this.stepOneForm.get('modelSelect')?.value,
              color: this.stepOneForm.get('colorSelect')?.value,
              config: '',
              towHitch: false,
              yoke: false
            });
          }
        }
      });

      this.stepService.updateStepOneValidity(this.stepOneForm.valid);
      const model = this.stepOneForm.get('modelSelect')?.value;
      this.colors.set(this.models().find(modelObj => modelObj.code === model)?.colors ?? []);

      if (model) {
        const shoppingCart = this.stepService.shoppingCart;
        this.stepService.updateShoppingCart({
          model: this.stepOneForm.get('modelSelect')?.value,
          color: this.stepOneForm.get('colorSelect')?.value,
          config: shoppingCart.config ?? '',
          towHitch: shoppingCart.towHitch ?? false,
          yoke: shoppingCart.yoke ?? false
        });
      }
    })
  }

  onModelSelected() {
    this.stepOneForm.get('colorSelect')?.setValue(this.colors()[0]?.code || '');
  }

  patchForm() {
    const shoppingCart = this.stepService.shoppingCart;
    this.stepOneForm.setValue({
      modelSelect: shoppingCart.model,
      colorSelect: shoppingCart.color
    })
  }
}
