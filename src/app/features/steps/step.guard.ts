import {CanActivateFn, Router} from '@angular/router';
import {inject} from "@angular/core";
import {StepService} from "./step.service";

export const stepGuard: CanActivateFn = (route, state) => {
  const stepService: StepService = inject(StepService);
  const router: Router = inject(Router);
  const url = route.routeConfig?.path;
  const urlTreeToRedirect = router.createUrlTree(['/step/1']);

  if (url === 'step/2') {
    if (stepService.stepOneValidity()) {
      return true;
    }
  }

  if (url === 'step/3') {
    if (stepService.stepTwoValidity()) {
      return true;
    }
  }

  return urlTreeToRedirect;
};
