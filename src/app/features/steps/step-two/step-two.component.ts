import {Component, DestroyRef, inject, OnInit} from '@angular/core';
import {Option} from "../../car";
import {FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {StepService} from "../step.service";
import {CurrencyPipe} from "@angular/common";
import {takeUntilDestroyed} from "@angular/core/rxjs-interop";

@Component({
  selector: 'app-step-two',
  standalone: true,
  imports: [ReactiveFormsModule, CurrencyPipe],
  templateUrl: './step-two.component.html',
  styleUrl: './step-two.component.scss'
})
export class StepTwoComponent implements OnInit {
  private readonly destroyRef = inject(DestroyRef);
  private readonly stepService = inject(StepService);
  options!: Option;
  stepTwoForm: FormGroup = new FormGroup({
    configSelect: new FormControl('', Validators.required),
    includeTow: new FormControl(false),
    includeYoke: new FormControl(false)
  });
  configMetrics!: { range: number, speed: number, price: number };

  ngOnInit() {
    this.stepService.getOptions()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(options => {
      this.options = options;
      this.getConfigMetrics();
      this.patchForm();
    });

    this.stepTwoForm.valueChanges
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(() => {
      this.stepService.updateStepTwoValidity(this.stepTwoForm.valid);
      this.stepService.updateShoppingCart({
        model: this.stepService.shoppingCart.model,
        color: this.stepService.shoppingCart.color,
        config: this.stepTwoForm.get('configSelect')?.value,
        towHitch: this.stepTwoForm.get('includeTow')?.value ?? false,
        yoke: this.stepTwoForm.get('includeYoke')?.value ?? false
      });
    });
  }

  getConfigMetrics() {
    const configId = this.stepService.shoppingCart.config;
    const config = this.options.configs.find(configObj => configObj.id === +configId);

    if (config) {
      this.configMetrics = {
        range: config.range,
        speed: config.speed,
        price: config.price
      }
    }
  }

  patchForm() {
    const shoppingCart = this.stepService.shoppingCart;
    this.stepTwoForm.patchValue({
      configSelect: shoppingCart.config,
      includeTow: shoppingCart.towHitch,
      includeYoke: shoppingCart.yoke
    })
  }
}
