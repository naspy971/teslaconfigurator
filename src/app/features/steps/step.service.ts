import {inject, Injectable, signal, WritableSignal} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Option, Model} from "../car";
import {Observable} from "rxjs";
import {ShoppingCart} from "../../shared/interfaces/shopping-cart.interface";

@Injectable({
  providedIn: 'root'
})
export class StepService {
  private http = inject(HttpClient);
  private _shoppingCart: WritableSignal<ShoppingCart> = signal({
    model: '',
    color: '',
    config: '',
    towHitch: false,
    yoke: false
  });

  get shoppingCart(): ShoppingCart {
    return this._shoppingCart();
  }

  stepOneValidity = signal(false);
  stepTwoValidity = signal(false);

  getModels(): Observable<Model[]> {
    return this.http.get<Model[]>('/models');
  }

  getOptions(): Observable<Option> {
    const model = this._shoppingCart().model;
    return this.http.get<Option>(`/options/${model}`);
  }

  updateShoppingCart(updatedShoppingCart: ShoppingCart): void {
    this._shoppingCart.set(updatedShoppingCart);
  }

  updateStepOneValidity(validity: boolean): void {
    this.stepOneValidity.set(validity);
  }

  updateStepTwoValidity(validity: boolean): void {
    this.stepTwoValidity.set(validity);
  }

  getCarImagePath(): string {
    const {model, color} = this._shoppingCart();
    return `https://interstate21.com/tesla-app/images/${model}/${color}.jpg`;
  }
}
