import {OptionConfig, Color} from "../../car";

export interface Summary {
  carModel: string;
  config: OptionConfig;
  totalCost: number;
  color: Color;
}
