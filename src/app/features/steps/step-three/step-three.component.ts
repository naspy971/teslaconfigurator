import {Component, DestroyRef, inject, OnInit, signal, WritableSignal} from '@angular/core';
import {CommonModule} from "@angular/common";
import {StepService} from "../step.service";
import {switchMap} from "rxjs/operators";
import {forkJoin, of} from 'rxjs';
import {Summary} from "./summary.interface";
import {Color, OptionConfig} from "../../car";
import {takeUntilDestroyed} from "@angular/core/rxjs-interop";

@Component({
  selector: 'app-step-three',
  standalone: true,
  imports: [
    CommonModule,
  ],
  templateUrl: './step-three.component.html',
  styleUrl: './step-three.component.scss'
})
export class StepThreeComponent implements OnInit {
  private readonly destroyRef = inject(DestroyRef);
  protected readonly stepService = inject(StepService);

  summary: WritableSignal<Summary> = signal({
    carModel: '',
    color: {} as Color,
    config: {} as OptionConfig,
    totalCost: 0
  });
  extraOptionCost = signal(1000);

  ngOnInit() {
    forkJoin({
      models: this.stepService.getModels(),
      options: this.stepService.getOptions()
    }).pipe(
      switchMap(({models, options}) => {
        const shoppingCart = this.stepService.shoppingCart;
        let totalCost = 0;
        const selectedModel = models.find(model => model.code === shoppingCart.model);
        const selectedConfigId = +shoppingCart.config!;
        const selectedColorCode = shoppingCart.color;
        const carModel = selectedModel ? selectedModel.description : '';
        const color = selectedModel?.colors.find(color => color.code === selectedColorCode) || {} as Color;
        const config = options?.configs.find(configObj => configObj.id === selectedConfigId) || {} as OptionConfig;

        totalCost += config.price;
        totalCost += color.price;
        totalCost += shoppingCart.towHitch ? 1000 : 0;
        totalCost += shoppingCart.yoke ? 1000 : 0;

        return of({carModel, color, config, totalCost});
      })
    )
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(({carModel, color, config, totalCost}) => {
      this.summary.set({carModel, color, config, totalCost});
    });
  }
}
