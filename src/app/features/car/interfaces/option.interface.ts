import {OptionConfig} from "./option-config.interface";

export interface Option {
  configs: OptionConfig[];
  towHitch: boolean;
  yoke: boolean;
}
