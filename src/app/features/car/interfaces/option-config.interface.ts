export interface OptionConfig {
  id: number;
  description: string;
  price: number;
  range: number;
  speed: number;
}
