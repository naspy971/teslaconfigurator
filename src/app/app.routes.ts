import {Routes} from '@angular/router';
import {StepOneComponent} from "./features/steps/step-one/step-one.component";
import {StepTwoComponent} from "./features/steps/step-two/step-two.component";
import {StepThreeComponent} from "./features/steps/step-three/step-three.component";
import {stepGuard} from "./features/steps/step.guard";

export const routes: Routes = [
  {path: '', redirectTo: 'step/1', pathMatch: 'full'},
  {path: 'step/1', component: StepOneComponent},
  {path: 'step/2', component: StepTwoComponent, canActivate: [stepGuard]},
  {path: 'step/3', component: StepThreeComponent, canActivate: [stepGuard]},
  {path: '**', redirectTo: 'step/1'},
];
